const express = require('express');
const mongoose = require('mongoose');
const config = require('./config');
const connectDB = require('./config/database');

//Connect database
connectDB(config.mongoURI);

//Create global
const app = express();
app.use(express.json())

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, token");
    res.header("Access-Control-Allow-Methods", "Origin, GET, POST, PUT, DELETE, PATCH");
    next();
});


//Route Middlewares
app.use('/api', require('./routes'));


const port = process.env.PORT || config.port;

app.listen(port, () => {
    console.log(`Server running on port ${port}`)
})

//Morgan give infomation about each request