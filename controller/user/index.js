const _ = require('lodash')
const config = require('../../config')
const { createToken } = require('../../middlewares/createToken');
const { cryptPassword, comparePassword } = require('../../middlewares/password');
const { User } = require('../../models/User');

/**
 * @param: Information's user
 * @author: Luu Phuc
 */
module.exports.register = async (req, res, next) => {
    const { name, email, dateOfBirth, gender, address, phone, description } = req.body;
    try {
        const passwordHash = await cryptPassword(req.body.password)
        const newUser = new User({ ...req.body, password: passwordHash })
        newUser.save()
        return Promise.resolve({ message: 'Register successful' })
    } catch (e) {
        return Promise.reject(e)
    }
}

module.exports.login = async (req, res, next) => {
    const { email, password } = req.body;
    try {
        const user = await User.findOne({ email });

        const isMatched = await comparePassword(req.body.password, user.password);
        if (!isMatched) return Promise.reject({ message: 'Wrong password' });

        const payload = _.pick(user, ['_id', 'role', 'email', 'name'])
        const token = await creatxxeToken(payload, '1h');

        return res.status(200).json({ message: 'Login successful', token })

    } catch (e) {
        return Promise.reject(e)
    }
}