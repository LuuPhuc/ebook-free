import CoreService from '../core/coreRequest';

const core = CoreService.getInstance();

class AuthService {
    async loginInternal(params) {
        try {
            if (!params) return Promise.reject({ message: 'Paramas not found' })
            let url = 'api/login';
            return await core.doRequest({ url, method: 'post', body: params, options: { unAuth: true } });
        } catch (e) {
            return Promise.reject(e);
        }
    }
}

export default AuthService;