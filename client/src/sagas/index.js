import { all, takeLatest, takeEvery } from 'redux-saga/effects';

/* ------------- Types ------------- */
import { AuthTypes } from '../store/ducks/auth.duck';

/* ------------- Sagas ------------- */
import { login } from './auth.saga';


/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.

/* ------------- Connect Types To Sagas ------------- */
export default function* rootSaga() {
    yield all([
        // auth
        takeLatest(AuthTypes.LOGIN_REQUEST, login),
    ]);
}