import { call, put } from 'redux-saga/effects';

import AuthService from '../services/auth.service';
import AuthActions from '../store/ducks/auth.duck';

const api = new AuthService();

export function* login(action) {
    const { requestType, params } = action;
    try {
        let result = yield call(api.loginInternal, params);

        if (result && result.status === 999) {
            yield put(AuthActions.loginSuccess(requestType, result));
        }
        throw result;
    } catch (e) {
        yield put(AuthActions.commonAuthFailure(requestType, e));
    }
}