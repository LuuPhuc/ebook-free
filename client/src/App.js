import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import Login from './screens/auth/Login';
import Home from './screens/home/Home';

export default function App({ store }) {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Switch>
                    <Route path='/' exact component={Home} />
                    <Route path='/login' exact component={Login} />
                </Switch>
            </BrowserRouter>
        </Provider>
    )
}