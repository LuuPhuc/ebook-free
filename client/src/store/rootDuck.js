import { combineReducers } from 'redux';

export const rootReducer = combineReducers({
    auth: require('./ducks/auth.duck').reducer
})