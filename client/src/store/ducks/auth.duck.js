import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

// Types and Action Creators--------------------
const { Types, Creators } = createActions({
    commonAuthSuccess: ['requestType', 'payload'],
    commonAuthFailure: ['requestType', 'error'],

    loginRequest: ['requestType', 'params'],
    loginSuccess: ['requestType', 'payload'],

})

export const AuthTypes = Types;
export default Creators;

// Initial State -------------------------------
export const INITIAL_STATE = Immutable({
    error: {},
    fetching: {},
    content: {},

    user: undefined,
    authToken: undefined
});

// START: Common action
export const commonAuthSuccess = (state, { requestType, payload }) => {
    return state.merge({
        fetching: { ...state.fetching, [requestType]: false },
        content: { ...state.content, [requestType]: payload },
    });
}

export const commonAuthFailure = (state, { requestType, error }) => {
    return state.merge({
        fetching: { ...state.fetching, [requestType]: false },
        error: { ...state.error, [requestType]: error },
    });
}
// END: Common action

// resetSourceData
export const resetSourceData = state => {
    return state.merge({
        error: {},
        fetching: {},
        content: {},

        user: undefined,
        authToken: undefined
    });
};

// Login
export const loginRequest = (state, { requestType, params }) => {
    return state.merge({
        fetching: { ...state.fetching, [requestType]: true },
        error: { ...state.error, [requestType]: null },
    });
}

export const loginSuccess = (state, { requestType, payload }) => {
    let { accessToken } = payload;
    return state.merge({
        user: payload,
        authToken: accessToken,
        fetching: { ...state.fetching, [requestType]: false },
    });
}

export const reducer = createReducer(INITIAL_STATE, {

    [Types.COMMON_AUTH_SUCCESS]: commonAuthSuccess,
    [Types.COMMON_AUTH_FAILURE]: commonAuthFailure,

    [Types.LOGIN_REQUEST]: loginRequest,
    [Types.LOGIN_SUCCESS]: loginSuccess,

});