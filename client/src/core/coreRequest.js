import requestHandler from '../services/requestHandler';
import axios from 'axios';

const apiRequestHandler = new requestHandler();

class CoreRequest {

    static instance = null;
    static createInstance() {
        let object = new CoreRequest();
        return object;
    }
    static getInstance() {
        if (!CoreRequest.instance) {
            CoreRequest.instance = CoreRequest.createInstance();
            return CoreRequest.instance;
        }
    }

    async doRequest({ method, url, body, options }) {
        let { header } = options || {};
        let reqHeader = { 'Content-Type': header === 'upload' ? 'multipart/form-data' : 'application/json' }
        if (header && header !== 'upload') { reqHeader = { ...reqHeader, ...header } }
        try {
            let data = await apiRequestHandler.handleRequest(
                method, url, body, reqHeader, options
            )
            return data;
        } catch (e) {
            return Promise.reject(e)
        }
    }
}

export default CoreRequest;