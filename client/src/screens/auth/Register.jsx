import React, { useState } from 'react';

import MyInput from '../../components/input';
import MyForm from '../../components/form';
const Register = () => {
    const [formData, setFormData] = useState({
        name: '',
        email: '',
        password: '',
        passwordConfirm: '',
        phone: '',
        gender: '',
        dateOfBirth: '',
        address: '',
        description: ''
        // textChange: 'Sign Up'
    });

    const { name, email, password, passwordConfirm, phone, gender, dateOfBirth, address, description } = formData;

    const handleChange = text => e => {
        setFormData({ ...formData, [text]: e.target.value });
    }

    const handleSubmit = e => {
        e.preventDefault();

    }

    return (
        <div className='min-h-screen bg-gray-100 text-gray-900 flex justify-center'>
            <div className='lg:w-1/2 xl:w-5/12 p-6 sm:p-12'>
                <div className='mt-12 flex flex-col items-center'>
                    <h1 className='text-2xl xl:text-3xl font-extrabold'>
                        Sign Up
                    </h1>

                    <MyForm onSubmit={handleSubmit}>
                        <MyInput
                            type='text'
                            placeholder='Name'
                            onChange={handleChange('name')}
                            value={name}
                        />
                        <MyInput
                            type='text'
                            placeholder='Email'
                            onChange={handleChange('name')}
                            value={email}
                        />
                    </MyForm>
                </div>
            </div>
        </div>
    )
}   
export default Register;
