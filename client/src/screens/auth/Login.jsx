import React, { useState, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Formik } from 'formik';
import { connect } from 'react-redux';
import { TextField, Button, FormControl } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import authActions from '../../store/ducks/auth.duck';


function Login(props) {

    const onSubmit = (values, { setStatus, setSubmitting }) => {
        props.login('login', values);
    }

    return (
        <Fragment>
            <div className='login-head'>
                <span>Do you have account</span>
                <Link to='/register'>Sign up</Link>
            </div>
            <div className='login-body'>
                <div className='login-form'>
                    <h3>Login</h3>
                    <Formik
                        initialValues={{ email: '', password: '' }}
                        onSubmit={(values, { setStatus, setSubmitting }) => onSubmit(values, { setStatus, setSubmitting })}
                    >
                        {({ values, status, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
                            <FormControl
                                noValidate={true}
                                onSubmit={handleSubmit}
                            >
                                <div className='form-group'>
                                    <TextField
                                        id="filled-password-input"
                                        variant="outlined"
                                        type='email'
                                        label='Email'
                                        margin='normal'
                                        name='email'
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.email}
                                    />
                                </div>
                                <div className='form-group'>
                                    <TextField
                                        id="filled-password-input"
                                        variant="outlined"
                                        type='password'
                                        label='Password'
                                        margin='normal'
                                        name='password'
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.password}
                                    />
                                </div>
                                <div className='login-action'>
                                    <Link to='/forgot-password'></Link>
                                    <Button type='submit' disabled={isSubmitting} variant="contained" color="primary" size="large">Login</Button>
                                </div>
                            </FormControl>
                        )}
                    </Formik>
                    <div className='login-option'>
                        <Link to="http://facebook.com" className='btn btn-primary'>
                            <i className="fab fa-facebook-f" />
                            Facebook
                        </Link>
                        <Link to="http://twitter.com" className='btn btn-info'>
                            <i className="fab fa-twitter" />
                            Twitter
                        </Link>
                        <Link to="google.com" className='btn btn-danger'>
                            <i className="fab fa-google" />
                            Google
                        </Link>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

const mapDispatchToProps = dispatch => ({
    login: (requestType, params) => dispatch(authActions.loginRequest(requestType, params)),
});

export default connect(null, mapDispatchToProps)(Login);