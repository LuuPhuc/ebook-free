const express = require('express');
const router = express.Router();


// router.get('/', (req, res) => {
//     res.json({ message: 'Hello home page' });
// })

//User
router.use('/users', require('./user'))

// //Admin
// router.use('/admin', require('../routes/admin/login'));
// router.use('/admin', require('../routes/admin/add'));
// router.use('/admin', require('../routes/admin/form'));

module.exports = router;
