const mongoose = require('mongoose');
const validator = require('validator');

const UserSchema = new mongoose.Schema({
    role:
    {
        type: String,
        default: 'Normal'
    },
    name:
    {
        type: String,
        require: true
    },
    username:
    {
        type: String,
        lowercase: true,
        match: [/^[a-zA-Z0-9]+$/, 'Chỉ bao gồm ký tự a-Z, 0-9'],
        index: true,
        default: null
    },
    avatar:
    {
        type: String,
        default: null
    },
    email:
    {
        type: String,
        trim: true,
        require: true,
        unique: true,
        lowercase: true
    },
    password:
    {
        type: String,
        require: true
    },
    resetPassword: {
        type: String,
        default: null
    },
    phone:
    {
        type: String,
        unique: true,
        validate:
        {
            validator: validator.isMobilePhone
        },
        required: true
    },
    gender:
    {
        type: Number,
        require: true
    },
    dateOfBirth:
    {
        type: Date,
        require: true
    },
    address:
    {
        type: String,
        require: true
    },
    description:
    {
        type: String,
        default: null
    }
}, { timestamps: true });

const User = mongoose.model('User', UserSchema, 'User')

module.exports = {
    UserSchema, User
}
