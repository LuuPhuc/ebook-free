const jwt = require('jsonwebtoken');
const config = require('../config');
const User = require('../models/User');

module.exports.authenticate = (req, res, next) => {
    const token = req.headers['token'];
    if (token) {
        jwt.verify(token, config.secretKey, async (err, decoded) => {
            if (decoded) {
                req.user = decoded;
                return next();
            } else {
                return res.status(401).json({ message: 'Failed to authenticate token' })
            }
            return res.status(200).json({ message: 'Token is invalid' })
        })
        return res.status(401).json({ message: 'You must provide token' })
    });
}

module.exports.authorize = (role) => (req, res, next) => {
    const { role } = req.user;
    const index = role.findIndex(e => e === role);

    if (index > -1) return next();

    res.status(403).json({ message: 'You are not allowed to access' })
}